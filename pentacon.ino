#define APERTURE_VALUE_COUNT 13

int analogPin = 0;
int raw = 0;
int Vin = 5;
float Vout = 0;
float R1 = 220;
float R2 = 0;
float buffer = 0;


typedef struct aperture_resistance_map {
  float f;
  float maxR;
};

void setup(){
  Serial.begin(9600);
}

static const aperture_resistance_map aperture_values[] =
{
  { .f = 16,   .maxR = 200, },
  { .f = 13,   .maxR = 172, },
  { .f = 11,   .maxR = 169, }, // broken
  { .f = 9.5,  .maxR = 165, },
  { .f = 8,    .maxR = 159, },
  { .f = 6.7,  .maxR = 150, },
  { .f = 5.6,  .maxR = 142, },
  { .f = 4.8,  .maxR = 132, },
  { .f = 4,    .maxR = 120, },
  { .f = 3.3,  .maxR = 108, },
  { .f = 2.8,  .maxR = 92, },
  { .f = 2.2,  .maxR = 78, },
  { .f = 1.8,  .maxR = 60, },
};

float map_resistance_to_aperture(float r)
{
  int i = 0;
  for(i = APERTURE_VALUE_COUNT-1; i >= 0 ; i--)
  {
    if (r < aperture_values[i].maxR)
    {
      return aperture_values[i].f;
    }
  }
  return -1;
}

void loop(){
  raw = analogRead(analogPin);
  if(raw){
    buffer = raw * Vin;
    Vout = (buffer)/1024.0;
    buffer = (Vin/Vout) - 1;
    R2= R1 * buffer;
//    Serial.print("Vout: ");
//    Serial.println(Vout);
//    Serial.print("R2: ");
//    Serial.println(R2);
    //Serial.println(map(R2, 50, 176, 1.8, 16));
    Serial.print(R2);
    Serial.print(" : ");
    Serial.println(map_resistance_to_aperture(R2));
    delay(1000);
  }
}
